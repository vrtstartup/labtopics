import { Meteor } from 'meteor/meteor';
//import FacebookOAuthInit from './imports/oauth-facebook';
import SlackOAuthInit from './imports/oauth-slack';

import '../imports/api/users.js';
import '../imports/api/topics.js';

Meteor.startup(() => {
   SlackOAuthInit();
});
