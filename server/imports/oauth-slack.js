import { ServiceConfiguration } from 'meteor/service-configuration';
import { Meteor } from 'meteor/meteor';

const settings = Meteor.settings.oauth.slack;

const init = () => {
    if (!settings) return;
    ServiceConfiguration.configurations.upsert(
        { service: "slack" },
        {
            $set: {
                clientId: settings.appId,
                secret: settings.secret,
                loginStyle: 'popup'
            }
        }
    );
};

export default init;