import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';
import {createContainer} from 'meteor/react-meteor-data';

import '../api/users.js';

import {Topics} from '../api/topics.js';

import AccountsUIWrapper from './AccountsUIWrapper.jsx';
import Topic from './Topic.jsx';
import Modal from 'react-bootstrap-modal';
import nl2br from 'react-nl2br';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            crudAction: 'new',
            modalOpen: false,
            aanpak: 'vraag',
            tijdnodig: 'small'
        };
    }

    doLogOut() {
        Meteor.logout();
    }

    handleChange(event) {

        var state = {};
        state[event.target.id] = event.target.value;

        this.setState(state);
    }

    renderTopics() {
        console.log(this.props.currentUser._id);

        let filteredTopics = this.props.topics;

        return filteredTopics.map((topic) => {
            return (
                <div key={topic._id} className="topic well">
                    <h3>{topic.titel} {(topic.user._id == this.props.currentUser._id) ? (
                        <span><a href="#" className="btn btn-default btn-xs" onClick={this.openEditTopic.bind(this, topic._id)}>bewerk</a></span>
                    ) : ''}</h3>
                    <div>aanpak: {topic.aanpak} - tijd nodig: {topic.tijdnodig}</div>
                    <h4>Context:</h4>
                    <p>{nl2br(topic.input)}</p>
                    <h4>Onderwerp:</h4>
                    <p>{nl2br(topic.onderwerp)}</p>
                </div>
            );
        });
    }

    openEditTopic(topicId, event) {
        event.preventDefault();

        let filteredTopics = this.props.topics;
        let topic = _.where(filteredTopics, {_id: topicId});

        if (topic[0]) {
            topic = topic[0];
        }

        this.setState({
            modalOpen: true,
            crudAction: 'edit',
            aanpak: topic.aanpak,
            tijdnodig: topic.tijdnodig
        }, function () {
            ReactDOM.findDOMNode(this.refs._id).value = topic._id;
            ReactDOM.findDOMNode(this.refs.titel).value = topic.titel;
            ReactDOM.findDOMNode(this.refs.input).value = topic.input;
            ReactDOM.findDOMNode(this.refs.onderwerp).value = topic.onderwerp;
        });

    }

    openNewTopic(event) {
        event.preventDefault();

        this.setState({
            modalOpen: true,
            crudAction: 'new'
        }, function () {
            ReactDOM.findDOMNode(this.refs._id).value = '0';
            ReactDOM.findDOMNode(this.refs.titel).value = '';
            ReactDOM.findDOMNode(this.refs.input).value = '';
            ReactDOM.findDOMNode(this.refs.onderwerp).value = '';
        });
    }

    closeModal(event) {
        event.preventDefault();

        this.setState({modalOpen: false});
    }

    saveAndClose(event) {
        event.preventDefault();

        let action = this.state.crudAction;

        const topic = {
            titel: ReactDOM.findDOMNode(this.refs.titel).value.trim(),
            input: ReactDOM.findDOMNode(this.refs.input).value.trim(),
            aanpak: this.state.aanpak.trim(),
            onderwerp: ReactDOM.findDOMNode(this.refs.onderwerp).value.trim(),
            tijdnodig: this.state.tijdnodig.trim(),
        };

        if (action == 'new') {
            Meteor.call('topics.insert', topic);
        } else {
            let topicId = ReactDOM.findDOMNode(this.refs._id).value.trim();
            Meteor.call('topics.update', topic, topicId);
        }

        this.setState({modalOpen: false});
    }

    renderModal() {
        return (
            <Modal show={this.state.modalOpen} onHide={this.closeModal.bind(this)}>
                <div className="modal-header">
                    <a href="#" className="close" onClick={this.closeModal.bind(this)}><span>×</span></a>
                    <h3 className="modal-title">{ (this.state.crudAction == 'new') ? 'Nieuw' : 'Bewerk'} topic</h3>
                </div>
                <div className="modal-body">
                    <form className="new-topic form-horizontal">
                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="titel">Titel</label>
                            <div className="col-sm-10">
                                <input
                                    id="titel"
                                    type="text"
                                    ref="titel"
                                    className="form-control"
                                    placeholder="Vul een titel in dat je topic beschrijft"
                                />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="input">Input</label>
                            <div className="col-sm-10">
                                            <textarea
                                                id="input"
                                                ref="input"
                                                className="form-control"
                                                placeholder="Beschrijf kort de context waarbinnen dit project zich bevindt."
                                                rows="2"
                                            />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="aanpak">Aanpak</label>
                            <div className="col-sm-10">
                                <select id="aanpak" value={this.state.aanpak} className="form-control" onChange={this.handleChange.bind(this)}>
                                    <option value="brainstorm">Brainstorm</option>
                                    <option value="workshop">Workshop</option>
                                    <option value="vraag">Vraag</option>
                                    <option value="idee">Idee</option>
                                    <option value="anders">Iets anders</option>
                                </select>

                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="onderwerp">Onderwerp</label>
                            <div className="col-sm-10">
                                            <textarea
                                                id="onderwerp"
                                                ref="onderwerp"
                                                className="form-control"
                                                placeholder="Beschrijf je topic."
                                                rows="4"
                                            />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-sm-2" htmlFor="tijdnodig">Tijd nodig</label>
                            <div className="col-sm-10">
                                <select id="tijdnodig" value={this.state.tijdnodig} className="form-control" onChange={this.handleChange.bind(this)}>
                                    <option value="small">small (20 minuten)</option>
                                    <option value="large">large (40 minuten)</option>
                                </select>

                            </div>
                        </div>

                        <div className="form-group">
                            <div className="col-sm-10 col-sm-offset-2">
                                <input type="hidden" ref="_id"/>
                                <a href="#" className="btn btn-primary" onClick={this.saveAndClose.bind(this)}>Opslaan</a>
                            </div>
                        </div>
                    </form>
                </div>
            </Modal>
        );
    }

    render() {
        if (this.props.currentUser) {
            if (this.props.currentUser.services) {
                return (
                    <div className="container">
                        <div id="titleBar" className="row">
                            <div className="col-xs-12">
                                <h1>Lab dag: topics</h1>
                            </div>
                        </div>

                        {this.renderModal()}

                        <div className="row">
                            <div className="col-xs-12">
                                <p>Het doel van topics op een lab-dag is dat je zelf onderwerpen aanbrengt.<br />Deze onderwerpen leiden tot korte of lange sessies en kunnen verschillende vormen aannemen:
                                </p>
                                <ul>
                                    <li>
                                        <strong>brainstorm</strong>: je wil extra creatieve zuurstof over een onderwerp voor je project(en).
                                    </li>
                                    <li>
                                        <strong>workshop</strong>: je hebt iets gevonden waar je het met anderen over wil hebben om dingen aan elkaar wil leren.
                                    </li>
                                    <li>
                                        <strong>vraag</strong>: je wil iets bijleren of je eigen skills verbeteren binnen een concrete vaardigheid.
                                    </li>
                                    <li>
                                        <strong>idee</strong>: je hebt, los van een concreet project, zin om iets te proberen, te experimenteren.
                                    </li>
                                </ul>
                                <p>Op basis van alle onderwerpen en wie er kan, stellen we een schema samen van het verloop van de namiddag.</p>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-xs-12">
                                <h2>Topics
                                    <a href="#" className="btn btn-primary btn-xs" onClick={this.openNewTopic.bind(this)}>Voeg topic toe</a>
                                </h2>
                                {this.renderTopics()}
                            </div>
                        </div>

                        <div id="loginbar" className="row">
                            <div className="col-xs-12">
                                <p>Je bent ingelogd als {this.props.currentUser.profile.name} - {this.props.currentUser.profile.team}
                                    <a href="#" className="btn btn-default btn-xs" onClick={this.doLogOut.bind(this)}>log out</a>
                                </p>
                            </div>
                        </div>
                    </div>
                );
            } else {
                return (
                    <div>trouble in paradise, no service known.</div>
                )
            }
        } else {
            return (
                <div className="jumbotron vertical-center">
                    <div className="container">
                        <div className="col-xs-12">
                            <h1>Lab topics</h1>
                            <AccountsUIWrapper />
                        </div>
                    </div>
                </div>
            )
        }
    }
}

App.propTypes = {
    currentUser: PropTypes.object,
    topics: PropTypes.array.isRequired
};

export default createContainer(() => {
    Meteor.subscribe('userData');
    Meteor.subscribe('allTopicsForSession');

    return {
        currentUser: Meteor.user(),
        topics: Topics.find({}, {sort: {createdAt: -1}}).fetch()
    }
}, App);