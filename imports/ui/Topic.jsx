import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import nl2br from 'react-nl2br';

export default class Topic extends Component {

    render() {
        return (
            <div className="topic well">
                <h3>{this.props.topic.titel} <span>{this.props.topic.user.profile.name}</span></h3>
                <div>aanpak: {this.props.topic.aanpak} - tijd nodig: {this.props.topic.tijdnodig}</div>
                <h4>Context:</h4>
                <p>{nl2br(this.props.topic.input)}</p>
                <h4>Onderwerp:</h4>
                <p>{nl2br(this.props.topic.onderwerp)}</p>
            </div>
        );
    }
}

Topic.propTypes = {
    topic: PropTypes.object.isRequired,
};