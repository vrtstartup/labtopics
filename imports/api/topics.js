import {Meteor} from 'meteor/meteor';
import {Mongo} from 'meteor/mongo';
import {check} from 'meteor/check';

export const Topics = new Mongo.Collection('topics');

if (Meteor.isServer) {
    Meteor.publish('allTopicsForSession', function allTopicsForSessionPublication() {
        return Topics.find({});
    });
}

Meteor.methods({
    'topics.insert'(topic) {
        check(topic.titel, String);
        check(topic.input, String);
        check(topic.aanpak, String);
        check(topic.onderwerp, String);
        check(topic.tijdnodig, String);

        // Make sure the user is logged in before inserting a topic
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        var currentUser = Meteor.users.findOne(
            {
                _id: this.userId
            }, {
                fields: {
                    'profile': 1,
                    'services.slack': 1
                }
            }
        );

        topic.createdAt = new Date();
        topic.user = currentUser;

        Topics.insert(topic);
    },
    'topics.update'(topic, topicId) {
        check(topic.titel, String);
        check(topic.input, String);
        check(topic.aanpak, String);
        check(topic.onderwerp, String);
        check(topic.tijdnodig, String);

        // Make sure the user is logged in before inserting a topic
        if (!this.userId) {
            throw new Meteor.Error('not-authorized');
        }

        var topicUser = Topics.findOne(
            {
                _id: topicId
            }, {
                fields: {
                    'user._id': 1
                }
            }
        );

        if (this.userId !== topicUser.user._id) {
            throw new Meteor.Error('no-access');
        }

        topic.updatedAt = new Date();

        Topics.update({
                _id: topicId
            },
            {
                $set: topic
            }
        );
    }

});