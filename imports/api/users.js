import {Meteor} from 'meteor/meteor';

if (Meteor.isServer) {
    Meteor.publish("userData", function () {
        return Meteor.users.find(
            {
                _id: this.userId
            }, {
                fields: {
                    'profile': 1,
                    'services.facebook': 1
                }
            }
        );
    });
}